<?php
namespace Excellence\Crud\Block;

class Show extends \Magento\Framework\View\Element\Template
{
    protected $crudFactory;
    public $_coreRegistry;

    public function __construct(\Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Element\Template\Context $context,
        \Excellence\Crud\Model\CrudFactory $dataFactory,
        \Excellence\Crud\Model\HobbiesFactory $hobbiesFactory) {

        $this->hobbiesFactory = $hobbiesFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->crudFactory = $dataFactory;
        parent::__construct($context);
    }
    public function getUserData()
    {
        //get values of current page. if not the param value then it will set to 1
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit. if not the param value then it will set to 1
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 3;

        $collection = $this->crudFactory->create();
        $collection = $collection->getCollection();

        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }
}
